import { CustomRouter } from './infrastructure/router/CustomRouter';

function App() {
  return (
   <CustomRouter />
  );
}

export default App;
