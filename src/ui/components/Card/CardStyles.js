import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { breakpoints } from '../../constants/responsive-parameters';

export const CardWrapper = styled.div`
  display: grid;
  grid-template-rows: 210px 210px 80px;
  grid-template-areas: "image" "text" "stats";
  border-radius: 18px;
  background: #000;
  box-shadow: 5px 5px 15px rgba(0, 0, 0, 0.9);
  text-align: center;
  transition-timing-function: ease;
`;

export const CardImgWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const CardImage = styled.img`
  grid-area: image;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  border-radius: 10px;
`;

export const CardTextWrapper = styled.div`
  grid-area: text;
  margin: 25px;
`;

export const CardTextDate = styled.span`
  color: rgb(255, 7, 110);
  font-size: 13px;
`;

export const CardTextTitle = styled.h2`
  margin-top: 0px;
  font-size: 2rem;
  box-sizing: border-box;
  min-width: 0px;
  line-height: 1.2;
  margin: 0px;
  background: linear-gradient(
    110.78deg,
    rgb(118, 230, 80) -1.13%,
    rgb(249, 214, 73) 15.22%,
    rgb(240, 142, 53) 32.09%,
    rgb(236, 81, 87) 48.96%,
    rgb(255, 24, 189) 67.94%,
    rgb(26, 75, 255) 85.34%,
    rgb(98, 216, 249) 99.57%
  );
  background-clip: text;
  -webkit-background-clip: text;
  color: transparent;
  width: 14rem;

  @media (${breakpoints.device.xs}) {
    grid-template-columns: repeat(1, 1fr);
    width: 40rem;
  }
  @media (${breakpoints.device.sm}) {
    width: 18rem;
  }
  @media (${breakpoints.device.lg}) {
    width: 14rem;
  }
`;

export const CardStatWrapper = styled.div`
  grid-area: stats;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;
  background: #04bcff;
`;

export const CardStats = styled.div`
  color: white;
  padding: 10px;
`;

export const LinkText = styled(Link)`
  color: #fff;
  text-decoration: none;
`;