import Tilt from 'react-parallax-tilt';
import {
  CardImage, CardImgWrapper, CardStatWrapper, CardStats, CardTextDate,
  CardTextTitle, CardTextWrapper, CardWrapper, LinkText
} from './CardStyles';

export const Card = ({ podcast, buttonText = 'more', setSummary }) => {
  const { id, name, author, img, summary } = podcast;

  const truncate = (str) => {
    return str.length > 40 ? str.substring(0, 41) + '...' : str;
  };
  
  const handleCLick = () => {
    setSummary(summary);
  };
  return (
    <Tilt>
      <CardWrapper>
        <CardImgWrapper>
          <CardImage 
            src={ img } 
          />
        </CardImgWrapper>
        <CardTextWrapper>
          <CardTextDate>
            Author: { author }
          </CardTextDate>
          <CardTextTitle>
            { truncate(name) }
          </CardTextTitle>
        </CardTextWrapper>
        <CardStatWrapper>
          <CardStats>
            <LinkText 
              onClick={ handleCLick } 
              to={ '/podcast/' + id } 
            >
              { buttonText }
            </LinkText>
          </CardStats>
        </CardStatWrapper>
      </CardWrapper>
    </Tilt>
  );
};