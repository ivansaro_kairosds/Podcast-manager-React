import styled, { keyframes } from 'styled-components';

export const Loader = ({ text = 'Loading' }) => {
  return (
    <LoadingWrapper>
    <h3>{ text }</h3>
    <Dot delay='0s' />
    <Dot delay='0.1s' />
    <Dot delay='0.2s' />
  </LoadingWrapper>
  );
};

const LoadingWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: center;
  margin-top: 2rem;

`;

export const BounceAnimation = keyframes`
  0% { 
    margin-bottom: 0; 
  }

  50% { 
    margin-bottom: 1rem;
  }

  100% { 
    margin-bottom: 0;
  }
`;

const Dot = styled.div`
  background-color: #04bcff;
  border-radius: 50%;
  width: 0.75rem;
  height: 0.75rem;
  margin: 0 0.25rem;
  /*Animation*/
  animation: ${BounceAnimation} 0.5s linear infinite;
  animation-delay: ${(props) => props.delay};
`;