import styled from 'styled-components';

const SearchBox = ({ setSearch, placeholder = 'Filter Podcast', numberPodcast = 0 }) => {
  return (
    <Container>
      <Number>{ numberPodcast }</Number>
      <Input 
        onChange={
          (e) => setSearch(e.target.value.toLowerCase())
        }
        placeholder={ placeholder }
        type='text' 
      />
    </Container>
  );
};

const Input = styled.input`
display: block;
margin-right: 2rem;
margin-top: 1rem;
min-width: 10%;
padding: 3px 5px;
text-align: left;
`;

const Number = styled.span`
margin-left: auto;
margin-right: .7rem;
margin-top: 1rem;
border-radius: 30px;
background-color: #04bcff;
color: white;
padding: 2px 17px;
`;

const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
`;

export default SearchBox;
