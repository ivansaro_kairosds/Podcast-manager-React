/* eslint sort-keys: 0 */
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const PodcastDetailData = ({ episodes, id }) => {
  const truncate = (str) => {
    return str.length > 70 ? str.substring(0, 71) + '...' : str;
  };
  
  return (
    <DataContainer>
          <EpisodesContainerTitle>
            Episodes: { episodes.length }
          </EpisodesContainerTitle>
          <EpisodesContainerBody>
            <thead> 
                <TableHeaderRow> 
                  <HeaderItem1>Title</HeaderItem1> 
                  <HeaderItem2>Date</HeaderItem2> 
                  <HeaderItem3>Duration</HeaderItem3> 
                </TableHeaderRow> 
            </thead>
            <TableBody>
              {
                episodes && episodes.map(episode => { 
                  return <TableEpisodeRow key={ episode.id }> 
                          <BodyItem1>
                           <span title={ episode.title }>
                              <Link 
                                to={ `/podcast/${id}/episode/${episode.id}` }
                                style={ {
                                  'textDecoration':'none', 
                                  'listStyle': 'none', 
                                  'color':'#04bcff'
                                } }
                              >
                                { truncate(episode.title) }
                              </Link>
                           </span>
                          </BodyItem1> 
                          <BodyItem2>{ episode.date }</BodyItem2> 
                          <BodyItem3>{ episode.duration }</BodyItem3> 
                        </TableEpisodeRow>;}) 
              }
            </TableBody>
          </EpisodesContainerBody>
    </DataContainer>
  );
};

const DataContainer = styled.div`
width: 70%;
`;

const EpisodesContainerTitle = styled.h3`
font-size: 30px;
box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 15%);
padding: .7rem 0px .7rem 10px;
margin-bottom: 2rem;
`;

const EpisodesContainerBody = styled.table`
width: 100%;
display : flex;
flex-direction : column;
border: 1px solid #f2f2f2;
font-size: 1rem;
line-height: 1.5;
justify-content : space-between;
border-radius: 8px;
box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 15%);
`;
const TableBody = styled.tbody`
&:nth-of-type(even) {
background-color: #ffffff;

&:nth-of-type(odd) {
background-color: #f2f2f2;
}
`;

const TableHeaderRow = styled.tr`
display: flex;
flex-flow: row nowrap;
font-weight: 700;
border-bottom: 1px solid #E8E8E8;
padding: 10px;
`;
const TableEpisodeRow = styled.tr`
display: flex;
flex-flow: row nowrap;
padding: 10px;
`;

const HeaderItem1 = styled.th`
display : flex;
flex-grow :1;
padding: 0.5em;
flex-grow: 2.1;
width: 70%;
`;
const HeaderItem2 = styled.th`
display : flex;
flex-grow :1;
padding: 0.5em;
width: 15%;
`;

const HeaderItem3 = styled.th`
display : flex;
flex-grow :1;
padding: 0.5em;
width: 15%;
`;

const BodyItem1 = styled.td`
display : flex;
flex-grow :1;
padding: 0.5em;
flex-grow: 2.1;
text-align: center;
width: 70%;
color: #04bcff;
cursor: pointer;
&:hover {
  opacity: .7;
}
`;

const BodyItem2 = styled.td`
display : flex;
flex-grow :1;
padding: 0.5em;
text-align: center;
width: 15%;
`;

const BodyItem3 = styled.td`
display : flex;
flex-grow :1;
padding: 0.5em;
width: 15%;
padding-left: 1.5rem;
`;