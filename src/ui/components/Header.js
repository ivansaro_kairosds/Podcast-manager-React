import { FaPodcast } from 'react-icons/fa';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

export const Header = () => {
  return (
    <>
      <Nav>
        <NavLogo to='/'>
          <FaPodcast />
          <Title>
            Podcaster
          </Title>
        </NavLogo>
      </Nav>
    </>
  );
};

export const Nav = styled.nav`
    background: #04bcff;
    height: 85px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding: 0.2rem;
`;
export const NavLogo = styled(Link)`
  cursor: pointer;
  color: #fff;
  font-size: 2rem;
  text-decoration: none;
  margin-left: 2rem;
  display: flex;
  align-items: center;
`;

export const Title = styled.div`
  color: #fff;
  margin-left: 0.7rem;
  margin-right: auto;
`;