import styled from 'styled-components';
import { Card } from './Card';
import { breakpoints } from '../constants/responsive-parameters';

export const PodcastList = ({ podcasts, setSummary }) => {
  return (
    <Items>
    {
    podcasts && podcasts?.map(
        podcast => 
          <Card 
            key={ podcast.id } 
            podcast={ podcast } 
            setSummary={ setSummary } 
          />
        )
    }
  </Items>
  );
};

const Items = styled.article`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 4rem;
  grid-row-gap: 3rem;
  padding: 0 5%;
  margin-top: 2rem;


  @media (${breakpoints.device.xs}) {
  grid-template-columns: repeat(1, 1fr);
    
  }
  @media (${breakpoints.device.sm}) {
  grid-template-columns: repeat(3, 1fr);
  
  }
  @media (${breakpoints.device.lg}) {
  grid-template-columns: repeat(4, 1fr);
  }
`;
