/* eslint-disable jsx-a11y/media-has-caption */
import styled from 'styled-components';

export const PodcastPlayer = ({ episodeToPlay }) => {
  const { title, description, audio } = episodeToPlay[0];
  
  return (
    <DataContainer>
      <Title>{ title }</Title>
      <Description>{ description }</Description>
      <Audio controls>
        <source src={ audio } type='audio/mpeg' />
        Your browser does not support the audio element.
      </Audio>
    </DataContainer>
  );
};

const DataContainer = styled.div`
width: 70%;
box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 15%);
border-radius: 8px;
`;

const Title = styled.div`
font-size: 20px;
padding: 1rem 2rem .5rem 2rem;
font-weight: 900;
color: #04bcff;
`;

const Description = styled.div`
padding: .5rem 2rem .5rem 2rem;
font-weight: 100;
font-style: italic;
`;

const Audio = styled.audio`
padding: .5rem 2rem .5rem 2rem;
width: 100%;
background-color: white;
`;