import styled from 'styled-components';

export const DetailCard = ({ name, author, img, summary }) => {
  const truncate = (str) => {
    return str.length > 300 ? str.substring(0, 301) + '...' : str;
  };
  
  return (
    <DetailCardContainer>
      <Box>
        <ImgContainer>
          <div>
            <Skew>
              <Img 
                src={ img } 
                alt={ name } 
              />
            </Skew>
          </div>
        </ImgContainer>
        <TexContainer>
          <H3>{ name }</H3>
          <div>
            by { author }
          </div>
        <H5>Description: </H5>
        <Description>
          <span 
            title={ summary }
          >
            { truncate(summary) || 'Description not available' }
          </span>
        </Description>
        </TexContainer>
      </Box>
    </DetailCardContainer>
  );
};

const DetailCardContainer = styled.div`
width: 30%;
margin-right: 3rem;
display: flex;
justify-content: center;
text-align: center;
`;

const Box = styled.div`
display: inline-block;
width: 275px;
text-align: center;
`;

const ImgContainer = styled.div`
height: 230px;
width: 200px;
overflow: hidden;
border-radius: 0px 0px 20px 20px;
display: inline-block;
`;

const Skew = styled.div`
display: inline-block;
border-radius: 20px;
overflow: hidden;
padding: 0px;
font-size: 0px;
background: #c8c2c2;
height: 250px;
width: 200px;
`;

const Img = styled.img`
height: 250px;
margin: -10px 0px 0px 0px;
`;

const TexContainer = styled.div`
box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.2);
padding: 120px 20px 20px 20px;
border-radius: 20px;
background: #fff;
margin: -120px 0px 0px 0px;
line-height: 19px;
font-size: 14px;
height: 450px;
`;

const H3 = styled.h3`
margin: 20px 0px 10px 0px;
color: #04bcff;
font-size: 18px;
`;

const H5 = styled.h5`
color: #04bcff;
font-size: 14px;
margin-top: 1rem;
`;

const Description = styled.div`
cursor: help;
font-size: 14px;
`;