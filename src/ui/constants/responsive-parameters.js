/* eslint sort-keys: 0 */
const size = {
  xs: '280px',
  sm: '768px',
  lg: '1200px'
};
const device = {
  xs: `min-width: ${size.xs}`,
  sm: `min-width: ${size.sm}`,
  lg: `min-width: ${size.lg}`
};

export const breakpoints = { size, device };