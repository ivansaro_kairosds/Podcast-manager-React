import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { useGetFromApi } from '../../infrastructure/hooks/useGetFromAPI';
import { DetailCard } from '../components/DetailCard';
import { Loader } from '../components/Loader';
import { PodcastDetailData } from '../components/PodcastDetailData';
import { PodcastPlayer } from '../components/PodcastPlayer';

export const PodcastDetailView = ({ summary }) => {
  
  const { podcastId, episodeId } = useParams();
  const { items: podcast, error, loading } = useGetFromApi(podcastId);
  const { id, name, author, img, episodes } = podcast;
  let episodeToPlay;
  
  if(episodeId && episodes) {
    episodeToPlay = episodes.filter(episode => episode.id === episodeId );
  }

  return (
    <>
      { loading &&  <Loader 
                      text='Loading' 
                    /> }
      { error &&  <div>
                    There has been an error
                  </div> }
      { 
        !loading && 
        !error && 
        podcast && 
          <MainContainer>
            <DetailCard 
              name={ name } 
              author={ author }
              img={ img }
              summary={ summary } 
            />
            {
              episodeId ? 
                <PodcastPlayer 
                  episodeToPlay={ episodeToPlay } 
                /> : 
                <PodcastDetailData 
                  episodes={ episodes } 
                  id={ id } 
                />
            }
          </MainContainer> }
    </>
  );
};

const MainContainer = styled.div`
display: flex;
width: 90%;
margin: 0 auto;
margin-top: 2rem;
`;