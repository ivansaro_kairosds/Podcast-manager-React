import { useGetFromApi } from '../../infrastructure/hooks/useGetFromAPI';
import { useInputFilter } from '../../infrastructure/hooks/useinputFilter';
import { Loader } from '../components/Loader';
import { PodcastList } from '../components/PodcastList';
import SearchBox from '../components/SearchBox';

export const MainView = ({ setSummary }) => {
  const { items, error, loading } = useGetFromApi();
  const { dataFiltered, setSearch } = useInputFilter(items);
  const numberPodcast = dataFiltered?.length || items?.length;
  
  return (
    <>
      <SearchBox 
        setSearch={ setSearch } 
        numberPodcast={ numberPodcast } 
      />
      { loading && <Loader text='Loading' /> }
      { error && <div>There has been an error</div> }
      { !loading && !error && items &&  <PodcastList  
                                          podcasts={ dataFiltered || items }  
                                          setSummary={ setSummary } 
                                        /> 
      }
    </>
  );
};
