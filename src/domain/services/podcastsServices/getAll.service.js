/* eslint sort-keys: 0 */
import { dataStorage } from './../../../infrastructure/storage/storage';
import { podcastRepository } from '../../../infrastructure/repositories/podcasts.repository';
const DAY_IN_MILISECONDS = 86400000;
const STORAGE_KEY = 'podcasts';

export const getAll = async () => {
  const storage = dataStorage.get(STORAGE_KEY);
  
  const getFreshDataFromAPI = async () => {
    dataStorage.delete(STORAGE_KEY);
    const data = await podcastRepository.getAllPodcasts();
    dataStorage.save(STORAGE_KEY, { podcasts: data, time: Date.now() });
    return data;
  };

  if(!storage || storage.length === 0){
    return getFreshDataFromAPI();
  }

  const isDayPased = (time) => {
    return (new Date().getMilliseconds() - time) > DAY_IN_MILISECONDS;
  };
  return (storage && !isDayPased(storage.time)) ? storage.podcasts : getFreshDataFromAPI();
};