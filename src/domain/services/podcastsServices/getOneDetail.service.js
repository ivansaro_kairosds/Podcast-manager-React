/* eslint sort-keys: 0 */
import { dataStorage } from './../../../infrastructure/storage/storage';
import { podcastDetailRepository } from '../../../infrastructure/repositories/podcastDetail.repository';
const DAY_IN_MILISECONDS = 86400000;
const STORAGE_KEY = 'episode-';

export const getOne = async (id) => {
  
  const getFreshDataFromAPI = async () => {
    const data = await podcastDetailRepository.getOnePodcast(id);
    dataStorage.save(STORAGE_KEY + id, { episode: data, time: Date.now() } );
    return data;
  };

  const storage = dataStorage.get(STORAGE_KEY + id);
  if(storage.length === 0){return getFreshDataFromAPI();}
  
  const returnDataStoraged = () => {
    return storage.episode;
  };

  const hasDayPassedSInceStoraged = (new Date().getMilliseconds() - storage.time) > DAY_IN_MILISECONDS;
  
  if(hasDayPassedSInceStoraged) {
    dataStorage.delete(STORAGE_KEY + id);
    return getFreshDataFromAPI(id);
  } else {
    return returnDataStoraged();
  }
};
