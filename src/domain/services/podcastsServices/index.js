import { getAll } from './getAll.service';
import { getOne } from './getOneDetail.service';

export const podcastService = {
  getAllPodcasts : getAll,
  getOnePodcast: getOne
};