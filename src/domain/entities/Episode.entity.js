export class Episode{
  constructor( id, title, date, duration, description, audio ) {
    this.id = id,
    this.title = title,
    this.date = date,
    this.duration = duration;
    this.description = description;
    this.audio = audio;
  }
}