export class PodcastDetail{
  constructor( id, name, author, img, episodes ) {
    this.id = id,
    this.name = name,
    this.author = author,
    this.img = img;
    this.episodes = episodes;
  }
}