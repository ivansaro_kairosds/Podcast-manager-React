export class Podcast{
  constructor( id, name, author, img, summary ) {
    this.id = id,
    this.name = name,
    this.author = author,
    this.img = img;
    this.summary = summary;
  }
}