import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import episodesData from './../mocks/episodes.mock.json';
import { PodcastDetailData } from '../../ui/components/PodcastDetailData';
describe('SearchBox component', () => {

  test('should render component', () => {
    const id = '1123243232';

    render(
      <BrowserRouter>
        <PodcastDetailData 
          episodes={ episodesData }
          id={ id }
        />
      </BrowserRouter>
    );

    const episode1 = screen.getByText(/tank/i);
    const episode2 = screen.getByText(/stevie mackey/i);
    const episode3 = screen.getByText(/letoya luckett/i);
    const episode4 = screen.getByText(/steelo brim/i);
    const episode17 = screen.getByText(/jamie foxx/i);
    expect(episode1).toBeInTheDocument();
    expect(episode2).toBeInTheDocument();
    expect(episode3).toBeInTheDocument();
    expect(episode4).toBeInTheDocument();
    expect(episode17).toBeInTheDocument();
  });
});
