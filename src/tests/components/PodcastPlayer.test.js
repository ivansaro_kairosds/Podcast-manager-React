/* eslint sort-keys: 0 */
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { PodcastPlayer } from '../../ui/components/PodcastPlayer';

describe('PodcastPlayer component', () => {

  test('should render component', () => {
    const title = 'This is a title';
    const description = 'And this is a description';
    const audio = 'www.audio.com';
    const episodeToPlay = [{ title, description, audio }];
    render(
      <BrowserRouter>
        <PodcastPlayer 
          episodeToPlay={ episodeToPlay }
        />
      </BrowserRouter>
    );

    const titleInScreen = screen.getByText(/this is a title/i);
    const descriptionInScreen = screen.getByText(/and this is a description/i);
    expect(titleInScreen).toBeInTheDocument();
    expect(descriptionInScreen).toBeInTheDocument();
  });
});
