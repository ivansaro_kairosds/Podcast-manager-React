import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Header } from '../../ui/components/Header';

describe('Header component', () => {

    test('Should render component', () => {
      render(
        <BrowserRouter>
          <Header />
        </BrowserRouter>
      );

      const title = screen.getByText(/podcaster/i);
      expect(title).toBeInTheDocument();
    });
});
