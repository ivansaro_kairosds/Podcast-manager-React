import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import SearchBox from '../../ui/components/SearchBox';

describe('SearchBox component', () => {

  test('should render component', () => {

    render(
      <BrowserRouter>
        <SearchBox />
      </BrowserRouter>
    );

    const searchBox = screen.getByPlaceholderText(/filter podcast/i);
    expect(searchBox).toBeInTheDocument();
  });
});
