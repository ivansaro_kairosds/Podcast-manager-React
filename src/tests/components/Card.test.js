/* eslint sort-keys: 0 */
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Card } from '../../ui/components/Card';

describe('Card component', () => {

    const podcast = {
      id: '1286770630',
      name: 'No Jumper',
      author: 'Andrew Hickey',
      img: 'img',
      summary: 'xxx'
    };
  
    test('Should render component', () => {
      render(
        <BrowserRouter>
          <Card  podcast={ podcast } />
        </BrowserRouter>
      );

      const buttonText = screen.getByText(/more/i);
      const name = screen.getByText(/no jumper/i);
      const author = screen.getByText(/andrew hickey/i);
      
      expect(buttonText).toBeInTheDocument();
      expect(name).toBeInTheDocument();
      expect(author).toBeInTheDocument();
    });
});