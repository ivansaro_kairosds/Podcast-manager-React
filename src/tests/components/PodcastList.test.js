/* eslint sort-keys: 0 */
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { PodcastList } from '../../ui/components/PodcastList';

const podcasts = [
  {
      'id': '1311004083',
      'name': 'Broken Record with Rick Rubin, Malcolm Gladwell, Bruce Headlam and Justin Richmond',
      'author': 'Pushkin Industries',
      'img': 'png'
  },
  {
      'id': '1236941416',
      'name': 'Ebro in the Morning Podcast',
      'author': 'HOT 97\'s Ebro in the Morning',
      'img': 'png'
  },
  {
      'id': '460107093',
      'name': 'Markus Schulz presents Global DJ Broadcast',
      'author': 'Markus Schulz',
      'img': 'png'
  }
];

describe('PodcastList component', () => {
  
  test('should render component', () => {

    render(
      <BrowserRouter>
        <PodcastList podcasts={ podcasts }/>
      </BrowserRouter>
    );

    const name1 = screen.getByText(/rubin/i);
    const name2 = screen.getAllByText(/ebro/i);
    const name3 = screen.getByText(/global dj/i);
    expect(name1).toBeInTheDocument();
    expect(name2[0]).toBeInTheDocument();
    expect(name3).toBeInTheDocument();

    const author1 = screen.getByText(/pushkin/i);
    const author2 = screen.getByText(/97's/i);
    const author3 = screen.getAllByText(/markus schulz/i);
    expect(author1).toBeInTheDocument();
    expect(author2).toBeInTheDocument();
    expect(author3[1]).toBeInTheDocument();

  });
});
