import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { DetailCard } from '../../ui/components/DetailCard';

describe('SearchBox component', () => {

  test('should render component', () => {

    const img = 'xxx';
    const name = 'TDC';
    const author = 'Ángel Codón';
    const summary = 'This is a summaty, it is not the best but it is mine';
    render(
      <BrowserRouter>
        <DetailCard 
          img={ img }
          name={ name }
          author={ author }
          summary={ summary }
        />
      </BrowserRouter>
    );

    const nameCard = screen.getByText(/tdc/i);
    const authorCard = screen.getByText(/ángel codón/i);
    expect(nameCard).toBeInTheDocument();
    expect(authorCard).toBeInTheDocument();
  });
});
