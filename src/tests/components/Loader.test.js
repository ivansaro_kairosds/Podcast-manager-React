import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Loader } from '../../ui/components/Loader';

describe('Header component', () => {

    test('Should render component', () => {
      render(
        <BrowserRouter>
          <Loader text='Loading' />
        </BrowserRouter>
      );

      const title = screen.getByText(/Loading/i);
      expect(title).toBeInTheDocument();
    });
});