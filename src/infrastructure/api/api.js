/* eslint sort-keys: 0 */
const headers = {
  'Content-Type': 'application/json'
};

export const api = {
  getAllPodcasts: (async() => {
    const BASE_URL = 'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json';
    return fetchFromAPI(BASE_URL);
    
  }),
  getPodcastDetail: (async(id) => {
    const BASE_URL = `https://itunes.apple.com/lookup?id=${ id }&media=podcast&entity=podcastEpisode&limit=100`;
    return fetchFromAPI(BASE_URL);
  })
};

const fetchFromAPI = async (url) => {
  const response = await fetch(allowCors(url), {
    method: 'GET',
    headers
  });

  return await response.json(); 
};

const allowCors = (url) => {
  const ALL_ORIGINS_URL = `https://api.allorigins.win/get?url=${encodeURIComponent(url)}`;
   return ALL_ORIGINS_URL;
};