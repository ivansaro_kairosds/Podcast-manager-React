/* eslint sort-keys: 0 */
import { useEffect, useState } from 'react';
import { podcastService } from '../../domain/services/podcastsServices';

export const useGetFromApi = (id = undefined) => {
  const [itemsState, setItemsState] = useState({
    items: [],
    loading: true,
    error: false
  });
  const beginLoadProcess = () => {
    get(id);
  };
  
  const get = async (id) => {
    let response;
    try {
      (id) ? 
        response = await podcastService.getOnePodcast(id):
        response = await podcastService.getAllPodcasts();
        setItemsState({
        items: response,
        loading: false,
        error: false
      });
    } catch (error) {
        console.error(error);
        setItemsState({
          items: [],
          loading: false,
          error: true
        });
    }
  };
  
  useEffect(() => {
   beginLoadProcess();
  }, []);
  return itemsState;
};