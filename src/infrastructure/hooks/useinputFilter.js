import { useEffect, useState } from 'react';

export const useInputFilter = (items) => {
  
  const [search, setSearch] = useState('');
  
  let dataFiltered;
  
  if(search.length > 2){
    dataFiltered = items
                    .filter(
                      item => { return item.name
                                            .toLowerCase()
                                            .includes(search) || item.author.toLowerCase()
                                            .includes(search); });
  }

  useEffect(() => {
  }, [search]);

  return { dataFiltered, setSearch };
};