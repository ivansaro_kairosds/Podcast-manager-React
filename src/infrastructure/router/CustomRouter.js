import { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Header } from '../../ui/components/Header';
import { MainView } from '../../ui/views/MainView';
import { PageNotFound } from '../../ui/views/PageNotFound';
import { PodcastDetailView } from '../../ui/views/PodcastDetailView';

export const CustomRouter = () => {  
  const [summary, setSummary] = useState('');
  
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route  path='/' 
                element={ <MainView 
                            setSummary={ setSummary } 
                          /> 
                      } 
        />
        <Route  path='/podcast/:podcastId' 
                element={ <PodcastDetailView 
                            summary={ summary } 
                          /> 
                        } 
        />
        <Route path='/podcast/:podcastId/episode/:episodeId' 
               element={ <PodcastDetailView 
               summary={ summary } 
        /> } 
        />
        <Route path='*' element={ <PageNotFound /> } />
      </Routes>
    </BrowserRouter>
  );
};