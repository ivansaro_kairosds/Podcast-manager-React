/* eslint sort-keys: 0 */
export const dataStorage = {
  get: ((STORAGE_KEY) => {
    const data = window.localStorage.getItem(STORAGE_KEY);
    return (data) ? JSON.parse(data) : []; 
  }),
  save: ((STORAGE_KEY, item ) => {
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(item));
  }),
  delete: ((STORAGE_KEY) => {
    window.localStorage.removeItem(STORAGE_KEY);
  })
};