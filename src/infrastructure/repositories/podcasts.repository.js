import { Podcast } from '../../domain/entities/Podcast.entity';
import { api } from '../api/api';

export const podcastRepository = {
  getAllPodcasts: async () => {
    const data = await api.getAllPodcasts();
    const { contents } = data;
    const parsedData = JSON.parse( contents );
    const podcasts = parsedData.feed.entry;

    const normalizedPodcasts = podcasts.map(podcast => {
      return new Podcast(
        String(podcast.id.attributes['im:id']),
        podcast['im:name'].label,
        podcast['im:artist'].label,
        podcast['im:image'][2].label,
        podcast.summary.label
      );
    });
    return normalizedPodcasts;
  }
};