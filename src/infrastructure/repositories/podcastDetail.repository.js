import { Episode } from '../../domain/entities/Episode.entity';
import { PodcastDetail } from '../../domain/entities/PodcastDetail.entity';
import { api } from '../api/api';

export const podcastDetailRepository = {
  getOnePodcast: async (id) => {
    const data = await api.getPodcastDetail(id);
    const { contents } = data;
    const parsedData = JSON.parse( contents );
    const podcast = parsedData.results;

    const episodes = [...podcast];
    const values = episodes.shift();
    
    const normalizedEpisodes = episodes.map(episode => {
      return new Episode(
        String(episode.trackId),
        episode.trackName,
        episode.releaseDate.slice(0, 10),
        formatDuration(episode.trackTimeMillis),
        episode.description,
        episode.previewUrl
      );
    });

    const normalizedPodcast = new PodcastDetail(
      String(values.trackId),
      values.collectionName,
      values.artistName,
      values.artworkUrl600,
      normalizedEpisodes
    );
    return normalizedPodcast;
  }
};

const formatDuration = milliseconds => {
  const HOUR_IN_MILLISECONDS = 3600000;
  
  const durationInHours = milliseconds / HOUR_IN_MILLISECONDS;
  const duration = `${Math.floor(durationInHours)}:${Math.floor((durationInHours % 1) * 60)
    .toString()
    .padStart(2, '0')}`;

  return duration;
};