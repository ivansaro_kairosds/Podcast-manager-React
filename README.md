
# Project Title

Podcaster

# Description

This project was born with the idea of managing access and listening to a series of podcasts hosted on iTunes. Since we wanted to prioritize the speed of development while maintaining a standard of quality, we decided to use React for the development, React Testing Library was used for the tests, eslint was used for error correction, React-icons was used for the icons. The layout was done using styled-components.



## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

Go to the project directory

```bash
  cd podcast-manager-react
```





## Installation

Install podcast-manager-react with npm

```bash
  npm install podcast-manager-react
```
    
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### `npm run coverage`

Launches the test runner in the interactive watch mode and it gives us a percentage of coverage in our test suites.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run lint`

Launches eslint and we receive a log with the errors detected.\

### `npm run lint:fix`

Launches eslint and we receive a log with the errors detected after the corrections made by eslint.\

## Views

-   Mainview: Here you can see all podcasts in a list.\
    url: '/'
-   List of episodes: A list of episodes of a specific podcast.\
    url: '/podcast/:podcastId'
-   Podcast player: This is where you can listen to your favorite podcast!.\
    url: 'podcast/:podcastId/episode/_episodeId
-   Not found: A funny animation to tell users that the url it's not correct.|
    url: '/notfound' or '/not/found' or '/found/not' or...